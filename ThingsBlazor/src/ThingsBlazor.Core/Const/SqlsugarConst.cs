﻿#region copyright
//------------------------------------------------------------------------------
//  此代码版权声明为全文件覆盖，如有原作者特别声明，会在下方手动补充
//  此代码版权（除特别声明外的代码）归作者本人Diego所有
//  源代码使用协议遵循本仓库的开源协议及附加协议
//  Gitee源代码仓库：https://gitee.com/diego2098/ThingsBlazor
//  Github源代码仓库：https://github.com/kimdiego2098/ThingsBlazor
//  使用文档：https://diego2098.gitee.io/thingsgateway-docs/
//  QQ群：605534569
//------------------------------------------------------------------------------
#endregion

namespace ThingsBlazor.Core
{
    /// <summary>
    /// Sqlsugar系统常量类
    /// </summary>
    public class SqlsugarConst
    {
        /// <summary>
        /// 默认库ConfigId
        /// </summary>
        public const string DB_Default = "Default";

        /// <summary>
        /// 业务库Id
        /// </summary>
        public const string DB_CustomId = "ThingsBlazor";
    }
}