﻿#region copyright
//------------------------------------------------------------------------------
//  此代码版权声明为全文件覆盖，如有原作者特别声明，会在下方手动补充
//  此代码版权（除特别声明外的代码）归作者本人Diego所有
//  源代码使用协议遵循本仓库的开源协议及附加协议
//  Gitee源代码仓库：https://gitee.com/diego2098/ThingsBlazor
//  Github源代码仓库：https://github.com/kimdiego2098/ThingsBlazor
//  使用文档：https://diego2098.gitee.io/thingsgateway-docs/
//  QQ群：605534569
//------------------------------------------------------------------------------
#endregion

namespace ThingsBlazor.Web.Entry
{
    /// <inheritdoc cref="ISingleFilePublish"/>
    public class SingleFilePublish : ISingleFilePublish
    {
        /// <inheritdoc/>
        public Assembly[] IncludeAssemblies()
        {
            return Array.Empty<Assembly>();
        }

        /// <inheritdoc/>
        public string[] IncludeAssemblyNames()
        {
            return new[]
            {
            "ThingsBlazor.Foundation",
            "ThingsBlazor.Web.Foundation",
            "ThingsBlazor.Web.Page",
            "ThingsBlazor.Application",
            "ThingsBlazor.Core",
            "ThingsBlazor.Web.Rcl",
            "ThingsBlazor.Web.Rcl.Core",
            "ThingsBlazor.Web.Core"
        };
        }
    }
}